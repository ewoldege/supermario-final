package com.example.ezana.supermario;

import android.graphics.Bitmap;
import android.graphics.Rect;

/**
 * Created by Ezana on 5/10/2015.
 */
public class Mario {
    Bitmap picture;
    Rect rect;
    boolean isFire,isInvincible;
    int xPos; //position relative to the canvas
    int xPostrue; //real position
    int yPos;

    public Mario(int type, int x, int y){
        this.xPos = x;
        this.xPostrue = this.xPos;
        this.yPos = y;
        if(type==0)
            this.picture = MainActivity.bitmap_stationary;
        else if(type==1)
            this.picture = MainActivity.bitmap_running;
        else if(type==2)
            this.picture = MainActivity.bitmap_jumping;

        this.rect = new Rect(x,y-125,x+125,y);
    }
}
