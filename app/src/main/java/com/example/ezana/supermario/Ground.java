package com.example.ezana.supermario;

import android.graphics.Bitmap;
import android.graphics.Rect;

/**
 * Created by Ezana on 5/8/2015.
 */
public class Ground {
    Bitmap picture;
    Rect rectangle1,rectangle2;

    public Ground(){
        this.picture = MainActivity.bitmap_sand;

        rectangle1 = new Rect();
        rectangle2 = new Rect();
        this.rectangle1.top = MainActivity.height*2/3;
        this.rectangle1.bottom = MainActivity.height;
        this.rectangle1.left = 0;
        this.rectangle1.right = MainActivity.width;

        this.rectangle2.top = MainActivity.height*2/3;
        this.rectangle2.bottom = MainActivity.height;
        this.rectangle2.left = MainActivity.width;
        this.rectangle2.right = 2*MainActivity.width;
    }
}
