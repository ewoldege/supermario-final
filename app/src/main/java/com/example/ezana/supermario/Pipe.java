package com.example.ezana.supermario;

import android.graphics.Bitmap;
import android.graphics.Rect;

/**
 * Created by Ezana on 5/13/2015.
 */
public class Pipe extends SurfaceObjects{


    public Pipe(int x, int y){
        this.type = "Pipe";
        this.picture = MainActivity.bitmap_warp_pipe;
        this.xPos = x;
        this.xPostrue = this.xPos;
        this.yPos = y;
        this.width = 128;
        this.length= 175;
        this.rect = new Rect(xPos,yPos-this.length,xPos+this.width,yPos);
    }
}
