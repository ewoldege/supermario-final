package com.example.ezana.supermario;

import android.graphics.Rect;

/**
 * Created by Ezana on 5/14/2015.
 */
public class FinishLine extends SurfaceObjects {
    public FinishLine(int x, int y) {
        this.type = "FinishLine";
        this.picture=MainActivity.bitmap_finish_line;
        this.xPos=x;
        this.xPostrue=this.xPos;
        this.yPos=y;
        this.width=150;
        this.length=300;
        this.rect=new
        Rect(xPos, yPos-this.length, xPos+this.width, yPos);
    }
}
