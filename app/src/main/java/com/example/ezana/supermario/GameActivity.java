package com.example.ezana.supermario;

import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.media.MediaPlayer;
import android.view.SurfaceView;

import java.util.ArrayList;

/**
 * Created by Ezana on 4/17/2015.
 */
//CREATED A NEW ACTIVITY FILE IN ORDER TO HOST THE ACTUAL GAME ACTIVITY
public class GameActivity extends ActionBarActivity {
    public static int lives=3;
    public static int score;
    public static int width;
    public static int height;
    public static int diffFlag=1;
    private static final String TAG = "Specific Event Handling Messages";
    public GameActivity game = this;
    public static SuperMarioView view;
    public static SuperMarioView1 view1;
    public static SuperMarioView2 view2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width = size.x;
        height = size.y;

        view = new SuperMarioView(getBaseContext());//Level 2
        view1 = new SuperMarioView1(getBaseContext());//Level 1
        view2 = new SuperMarioView2(getBaseContext());//Level 3

        ArrayList levels = new ArrayList(3);
        levels.add(view1);
        levels.add(view);
        levels.add(view2);

        if(MainActivity.level==1)
            setContentView ( (SurfaceView)levels.get(0) ) ;
        else if(MainActivity.level==2)
            setContentView ( (SurfaceView)levels.get(1) ) ;
        else if(MainActivity.level==3)
            setContentView ( (SurfaceView)levels.get(2) ) ;
        Intent i = new Intent(this, MainActivity.class);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}


