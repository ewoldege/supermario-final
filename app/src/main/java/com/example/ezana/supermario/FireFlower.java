package com.example.ezana.supermario;

import android.graphics.Rect;

/**
 * Created by Ezana on 5/18/2015.
 */
public class FireFlower extends SurfaceObjects {
    public FireFlower(int x, int y){
        this.type = "FireFlower";
        this.picture = MainActivity.bitmap_fire_flower;
        this.xPos = x;
        this.xPostrue = this.xPos;
        this.yPos = y;
        this.width = 128;
        this.length = this.width;
        this.rect = new Rect(xPos,yPos-length,xPos+width,yPos);
    }
    public void move(int vel){
        if((count/60)%2==1)
            vel = -vel;
        this.xPostrue+=vel;
        this.xPos+=vel;
        this.rect.left+=vel;
        this.rect.right+=vel;
        count++;

    }
}
