package com.example.ezana.supermario;

import android.graphics.Rect;

/**
 * Created by Ezana on 5/18/2015.
 */
public class Coin extends SurfaceObjects {
    public boolean gone;
    public Coin(int x, int y){
        this.type = "Coin";
        this.picture = MainActivity.bitmap_coin;
        this.xPos = x;
        this.xPostrue = this.xPos;
        this.yPos = y;
        this.width = 128;
        this.length = this.width;
        this.rect = new Rect(xPos,yPos-length,xPos+width,yPos);
    }
}
