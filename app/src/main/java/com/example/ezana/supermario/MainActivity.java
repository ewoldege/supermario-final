package com.example.ezana.supermario;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.TypedValue;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;


public class MainActivity extends ActionBarActivity {
    public static int width;
    public static int height;
    public static Bitmap bitmap_sand,bitmap_grass,bitmap_boo,bitmap_jumping,bitmap_running,bitmap_stationary,
                  bitmap_arrow_right,bitmap_arrow_left,bitmap_arrow_top,bitmap_arrow_down,bitmap_cloud,bitmap_goomba,bitmap_warp_pipe,bitmap_mario_running1,
            bitmap_mario_running2,bitmap_mario_running3,bitmap_mario_running1_1,bitmap_mario_running2_1,bitmap_mario_running3_1,bitmap_finish_line,bitmap_fire_flower,
            bitmap_bowser,bitmap_firemario_running1,bitmap_firemario_running2,bitmap_firemario_running3,bitmap_firemario_running1_1,bitmap_firemario_running2_1,bitmap_firemario_running3_1,
            bitmap_firemario_stationary,bitmap_fire,bitmap_coin,bitmap_block,bitmap_green_mario,bitmap_black_mario,bitmap_star;
    public static int level;
    @Override
    protected void onCreate ( Bundle savedInstanceState ) {
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width = size.x;
        height = size.y;
        super . onCreate ( savedInstanceState ) ;



        BitmapFactory. Options options = new BitmapFactory . Options () ;
        bitmap_sand = BitmapFactory. decodeResource(getResources(),
                R.drawable.mc_sand, options) ;
        bitmap_grass = BitmapFactory. decodeResource(getResources(),
                R.drawable.mc_grass, options) ;
        bitmap_boo = BitmapFactory. decodeResource(getResources(),
                R.drawable.boo, options) ;
        bitmap_jumping = BitmapFactory. decodeResource(getResources(),
                R.drawable.paper_mario, options) ;
        bitmap_running = BitmapFactory. decodeResource(getResources(),
                R.drawable.retro_mario, options) ;
        bitmap_stationary = BitmapFactory. decodeResource(getResources(),
                R.drawable.mario_stationary, options) ;
        bitmap_arrow_left = BitmapFactory. decodeResource(getResources(),
                R.drawable.arrow_left, options) ;
        bitmap_arrow_top = BitmapFactory. decodeResource(getResources(),
                R.drawable.arrow_up, options) ;
        bitmap_arrow_right = BitmapFactory. decodeResource(getResources(),
                R.drawable.arrow_right, options) ;
        bitmap_arrow_down = BitmapFactory. decodeResource(getResources(),
                R.drawable.arrow_down, options) ;
        bitmap_cloud = BitmapFactory. decodeResource(getResources(),
                R.drawable.cloud, options) ;
        bitmap_goomba = BitmapFactory. decodeResource(getResources(),
                R.drawable.goomba, options) ;
        bitmap_warp_pipe = BitmapFactory. decodeResource(getResources(),
                R.drawable.warp_pipe, options) ;
        bitmap_mario_running1 = BitmapFactory. decodeResource(getResources(),
                R.drawable.mario_running1, options) ;
        bitmap_mario_running2 = BitmapFactory. decodeResource(getResources(),
                R.drawable.mario_running2, options) ;
        bitmap_mario_running3 = BitmapFactory. decodeResource(getResources(),
                R.drawable.mario_running3, options) ;
        bitmap_firemario_running1 = BitmapFactory. decodeResource(getResources(),
                R.drawable.firemario_running1, options) ;
        bitmap_firemario_running2 = BitmapFactory. decodeResource(getResources(),
                R.drawable.firemario_running2, options) ;
        bitmap_firemario_running3 = BitmapFactory. decodeResource(getResources(),
                R.drawable.firemario_running3, options) ;
        bitmap_mario_running1_1 = BitmapFactory. decodeResource(getResources(),
                R.drawable.mario_running1_1, options) ;
        bitmap_mario_running2_1 = BitmapFactory. decodeResource(getResources(),
                R.drawable.mario_running2_1, options) ;
        bitmap_mario_running3_1 = BitmapFactory. decodeResource(getResources(),
                R.drawable.mario_running3_1, options) ;
        bitmap_firemario_running1_1 = BitmapFactory. decodeResource(getResources(),
                R.drawable.firemario_running1_1, options) ;
        bitmap_firemario_running2_1 = BitmapFactory. decodeResource(getResources(),
                R.drawable.firemario_running2_1, options) ;
        bitmap_firemario_running3_1 = BitmapFactory. decodeResource(getResources(),
                R.drawable.firemario_running3_1, options) ;
        bitmap_firemario_stationary = BitmapFactory. decodeResource(getResources(),
                R.drawable.firemario_stationary, options) ;
        bitmap_finish_line = BitmapFactory. decodeResource(getResources(),
                R.drawable.finish_line, options) ;
        bitmap_fire_flower = BitmapFactory. decodeResource(getResources(),
                R.drawable.fire_flower, options) ;
        bitmap_bowser = BitmapFactory. decodeResource(getResources(),
                R.drawable.paper_bowser, options) ;
        bitmap_fire = BitmapFactory. decodeResource(getResources(),
                R.drawable.retro_fire_ball, options) ;
        bitmap_coin = BitmapFactory. decodeResource(getResources(),
                R.drawable.question_coin, options) ;
        bitmap_block = BitmapFactory. decodeResource(getResources(),
                R.drawable.brick, options) ;
        bitmap_black_mario = BitmapFactory. decodeResource(getResources(),
                R.drawable.black_mario, options) ;
        bitmap_green_mario = BitmapFactory. decodeResource(getResources(),
                R.drawable.green_mario, options) ;
        bitmap_star = BitmapFactory. decodeResource(getResources(),
                R.drawable.star, options) ;

        RelativeLayout WelcomeScreen = new RelativeLayout(this);
        WelcomeScreen.setBackgroundColor(Color.WHITE);

        //Create some buttons for difficulty level
        Button easy = new Button(this);
        easy.setText("EASY");
        easy.setBackgroundColor(Color.WHITE);

        Button medium = new Button(this);
        medium.setText("MEDIUM");
        medium.setBackgroundColor(Color.YELLOW);

        Button hard = new Button(this);
        hard.setText("HARD");
        hard.setBackgroundColor(Color.RED);


        //Giving IDs to the buttons
        easy.setId(1);
        medium.setId(2);
        hard.setId(3);

        //Grabbing the dimensions
        RelativeLayout.LayoutParams easyButton = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT

        );

        RelativeLayout.LayoutParams mediumButton = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT

        );


        RelativeLayout.LayoutParams hardButton = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT

        );

        //Setting the placement rules. Easy button will be aligned in the center, but will be above medium
        //Hard will also be in the center but will be below medium
        //They both also have some space in between the buttons; hence, the need for margins.
        easyButton.addRule(RelativeLayout.CENTER_VERTICAL);
        easyButton.addRule(RelativeLayout.LEFT_OF,medium.getId());
        easyButton.setMargins(0,0,50,0);

        mediumButton.addRule(RelativeLayout.CENTER_HORIZONTAL);
        mediumButton.addRule(RelativeLayout.CENTER_VERTICAL);

        hardButton.addRule(RelativeLayout.CENTER_VERTICAL);
        hardButton.addRule(RelativeLayout.RIGHT_OF,medium.getId());
        hardButton.setMargins(50,0,0,0);

        //I used this tool in order to make this display the same across any device.
        Resources res = getResources();
        int pixelsW = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,200,
                res.getDisplayMetrics());
        int pixelsH = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,75,
                res.getDisplayMetrics());
        int pixelradius = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 25,
                res.getDisplayMetrics());


        easy.setWidth(pixelsW);
        medium.setWidth(pixelsW);
        hard.setWidth(pixelsW);

        easy.setHeight(pixelsH);
        medium.setHeight(pixelsH);
        hard.setHeight(pixelsH);

        //Added buttons along with their accompanying design rules to the main view.
        WelcomeScreen.addView(easy,easyButton);
        WelcomeScreen.addView(medium,mediumButton);
        WelcomeScreen.addView(hard,hardButton);

        //Set this view as the main view for this activity.
        setContentView(WelcomeScreen);

        //EVENT HANDLING
        easy.setOnClickListener(
                new Button.OnClickListener(){
                    public void onClick(View v){
                        level = 1;

                        Intent i = new Intent(getApplicationContext(), GameActivity.class);
                        startActivity(i);
                        //setContentView( new GameView( getBaseContext() ) );
                    }
                }
        );

        medium.setOnClickListener(
                new Button.OnClickListener(){
                    public void onClick(View v){
                        level = 2;

                        Intent i = new Intent(getApplicationContext(), GameActivity.class);
                        startActivity(i);
                        //setContentView( new GameView( getBaseContext() ) );
                    }
                }
        );

        hard.setOnClickListener(
                new Button.OnClickListener(){
                    public void onClick(View v){
                        level = 3;

                        Intent i = new Intent(getApplicationContext(), GameActivity.class);
                        startActivity(i);
                        //setContentView( new GameView( getBaseContext() ) );
                    }
                }
        );
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
