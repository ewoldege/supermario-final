package com.example.ezana.supermario;

import android.graphics.Rect;

/**
 * Created by Ezana on 5/18/2015.
 */
public class Fire extends SurfaceObjects {
    public Fire(int x, int y){
        this.type = "Fire";
        this.picture = MainActivity.bitmap_fire;
        this.xPos = x;
        this.xPostrue = this.xPos;
        this.yPos = y;
        this.width = 75;
        this.length = this.width;
        this.rect = new Rect(xPos,yPos-length,xPos+width,yPos);
    }
    public void move(int vel,int pos){
        if(this.xPostrue<=pos) {
            this.xPostrue += vel;
            this.xPos += vel;
            this.rect.left += vel;
            this.rect.right += vel;
        }
        else{
            this.xPostrue = pos;
        }
    }
}
