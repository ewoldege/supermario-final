package com.example.ezana.supermario;


import android.graphics.Canvas;
import android.graphics.Color;
import android.view.SurfaceHolder;

public class SuperMarioThread extends Thread {
    private final SuperMarioView view ;
    private static final int FRAME_PERIOD = 5; // In ms
    public SuperMarioThread ( SuperMarioView view ) {
        this . view = view ;
    }
    public void run () {

        SurfaceHolder sh = view . getHolder () ;
// Main game loop .
        while ( ! Thread . interrupted () ) {
            Canvas c = sh . lockCanvas ( null ) ;
            try {
                synchronized ( sh ) {
                    c.drawColor(Color.BLACK);
                    view . tick ( c ) ;

                }
            } catch ( Exception e ) {
            } finally {
                if ( c != null ) {
                    sh . unlockCanvasAndPost ( c ) ;
                }
            }
            try {
                Thread . sleep ( FRAME_PERIOD ) ;
            } catch ( InterruptedException e ) {
                return ;
            }
        }
    }
}

