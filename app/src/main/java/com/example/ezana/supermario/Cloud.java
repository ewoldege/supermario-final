package com.example.ezana.supermario;

import android.graphics.Bitmap;
import android.graphics.Rect;

/**
 * Created by Ezana on 5/12/2015.
 */
public class Cloud {
        int xPos;
        int yPos;
        Bitmap picture;
        Rect rect;

        public Cloud(int x, int y){
            this.picture = MainActivity.bitmap_cloud;
            this.xPos = x;
            this.yPos = y;
            this.rect = new Rect(xPos-128,yPos,xPos,yPos+128);
        }
}
