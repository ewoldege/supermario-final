package com.example.ezana.supermario;

import android.graphics.Canvas;

public interface TimeConscious {
    public void tick(Canvas canvas) ;
}
