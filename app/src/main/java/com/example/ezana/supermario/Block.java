package com.example.ezana.supermario;

import android.graphics.Bitmap;
import android.graphics.Rect;

/**
 * Created by Ezana on 5/13/2015.
 */
public class Block extends SurfaceObjects{
    boolean blockGone;

    public Block(int x, int y, int num){
        this.type = "Block";
        this.picture = MainActivity.bitmap_block;
        this.xPos = x;
        this.xPostrue = this.xPos;
        this.yPos = y;
        this.width = 128;
        this.length= 128;
        this.rect = new Rect(xPos,yPos-this.length,xPos+this.width,yPos);
        this.numBlock = num;
    }
}
