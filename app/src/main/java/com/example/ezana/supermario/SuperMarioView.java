package com.example.ezana.supermario;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.SystemClock;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.InputStream;
import java.util.ArrayList;


public class SuperMarioView extends SurfaceView implements SurfaceHolder. Callback , TimeConscious {

    private static final String TAG = "Specific Event Handling Messages";
    int touchCount;
    int velocity = MainActivity.width/25;

    ArrayList objects = new ArrayList();
    float xtouch,ytouch;
    int ticks;
    int tempCount;
    int jumpSpeed=0;
    int horizontalSpeed=0;
    int tempTick;
    int MarioDirection;//0 means LEFT 1 means RIGHT
    int collisionSide=4; //0 means LEFT 1 means RIGHT 2 means TOP 4 means nothing (just place holder)
    SurfaceObjects collidedObject;
    int alpha=255;
    int score=0;
    boolean booDead,goombaDead;
    boolean oncePer;
    boolean jumpFlag;
    boolean actionUp;
    boolean marioMoving;
    boolean collided;
    boolean gameOverFlag;
    boolean roundLostFlag;
    boolean completion;
    boolean scoreChanged;
    boolean playedBefore,playedBefore1,playedBefore2,playedBefore3,playedBefore4;
    boolean notAllowedRight,notAllowedLeft,notAllowedDown,notAllowedUp;
    boolean onlyOnce3,onlyOnce4,onlyOnce1,onlyOnce2,onlyOnce;
    Ground ground;
    Grass grass;
    Mario mario;
    Boo boo;
    Goomba goomba;
    Cloud cloud;
    Pipe pipe;
    FinishLine finishLine;
    Star star;
    Block[] block = new Block[3];
    MediaPlayer mySong,died,win,jump,stomp1,stomp2,coinSound;

    SuperMarioThread renderThread;

    public SuperMarioView ( Context context ) {
        super ( context ) ;
        getHolder () . addCallback(this) ;
        this.ground = new Ground();
        this.grass = new Grass();
        this.mario = new Mario(1,0,this.grass.rectangle1.top);
        this.boo = new Boo(MainActivity.width,this.grass.rectangle1.top);
        this.goomba = new Goomba((int)(1.5*MainActivity.width),this.grass.rectangle1.top);
        this.cloud = new Cloud(MainActivity.width,0);
        this.pipe = new Pipe(2*MainActivity.width,this.grass.rectangle1.top);
        this.finishLine = new FinishLine(3*MainActivity.width,this.grass.rectangle1.top);
        this.block[0] = new Block(5/2*MainActivity.width,this.grass.rectangle1.top-450,0);
        this.block[1] = new Block(5/4*MainActivity.width,this.grass.rectangle1.top-450,1);
        this.block[2] = new Block(7/4*MainActivity.width,this.grass.rectangle1.top-450,2);
        this.star = new Star(13/10*MainActivity.width,this.grass.rectangle1.top);
        objects.add(this.block[0]);
        objects.add(this.block[1]);
        objects.add(this.block[2]);
        objects.add(this.star);
        objects.add(this.boo);
        objects.add(this.goomba);
        objects.add(this.pipe);
        this.mySong = MediaPlayer.create(getContext(),R.drawable.smb_song);
        this.died = MediaPlayer.create(getContext(), R.drawable.smb_gameover);
        this.win = MediaPlayer.create(getContext(), R.drawable.smb_stage_clear);
        this.jump =MediaPlayer.create(getContext(), R.drawable.smb_jump_small);
        this.stomp1 =MediaPlayer.create(getContext(), R.drawable.smb3_stomp);
        this.stomp2 =MediaPlayer.create(getContext(), R.drawable.smb3_stomp);
        this.coinSound =MediaPlayer.create(getContext(), R.drawable.smb_coin_sound);
        this.died.setLooping(false);
        this.win.setLooping(false);
        this.jump.setLooping(false);
        if(died.isPlaying())
            died.pause();
        if(win.isPlaying())
            win.pause();
        if(mySong.isPlaying())
            mySong.pause();
        //mPlayer.start();



    }
    @Override
    public void surfaceCreated ( SurfaceHolder holder ) {
        renderThread = new SuperMarioThread ( this ) ;
        renderThread . start () ;





// Create the sliding background , cosmic factory , trajectory
// and the space ship

    }
    @Override
    public void surfaceChanged ( SurfaceHolder holder ,
                                 int format , int width , int height ) {
// Respond to surface changes , e . g . , aspect ratio changes .
    }

    @Override
    public void surfaceDestroyed ( SurfaceHolder holder ) {
// The cleanest way to stop a thread is by interrupting it .
// BubbleShooterThread regularly checks its interrupt flag .
        renderThread . interrupt () ;
    }
    @Override
    public boolean onTouchEvent ( MotionEvent e ) {
        switch ( e . getAction () ) {
            case MotionEvent . ACTION_DOWN :
                if(this.touchCount!=0) {

                    tempTick = this.ticks +50;
                    xtouch = e.getX();
                    ytouch = e.getY();
                }

                this.touchCount++;


                break ;
            case MotionEvent . ACTION_UP :
                this.touchCount++;
                actionUp = true;

                break ;
        }
        return true ;
    }
    @Override
    public void onDraw ( Canvas c ) {
        super . onDraw ( c ) ;
        Paint paint = new Paint();
        if(!gameOverFlag&&!roundLostFlag&&!completion) {
            paint.setAlpha(alpha);

            c.drawColor(Color.rgb(255, 165, 0));
        }
        else{
            if(alpha>=3)
            alpha-=3;
            else
            alpha=0;
            paint.setAlpha(alpha);
        }
        c.drawBitmap(MainActivity.bitmap_arrow_left,null,new Rect(0,128,128,256),paint);
        c.drawBitmap(MainActivity.bitmap_arrow_top,null,new Rect(128,0,256,128),paint);
        c.drawBitmap(MainActivity.bitmap_arrow_right,null,new Rect(256,128,3*128,256),paint);
        c.drawBitmap(this.cloud.picture,null,cloud.rect,paint);
        c.drawBitmap(this.ground.picture, null, ground.rectangle1, paint);
        c.drawBitmap(this.grass.picture, null, grass.rectangle1, paint);
        if(booDead){
            if (!playedBefore3) {
                this.stomp1.start();
                playedBefore3 = true;
            }
            boo.rect.top+=jumpSpeed;
            if(boo.rect.top >= grass.rectangle1.top){
                boo.rect.top = grass.rectangle1.top;
            }
        }
        if(goombaDead){
            if (!playedBefore4) {
                this.stomp2.start();
                playedBefore4 = true;
            }
            goomba.rect.top+=jumpSpeed;
            if(goomba.rect.top >= grass.rectangle1.top){
                goomba.rect.top = grass.rectangle1.top;
            }
        }
        c.drawBitmap(this.boo.picture, null, boo.rect, paint);
        if(!mario.isInvincible)
            c.drawBitmap(this.star.picture, null, star.rect, paint);

        c.drawBitmap(this.goomba.picture, null, goomba.rect, paint);
        c.drawBitmap(this.pipe.picture, null, pipe.rect, paint);
        c.drawBitmap(this.finishLine.picture,null,finishLine.rect,paint);

        Paint paintText = new Paint();
        paintText.setColor(Color.GRAY);
        paintText.setTextSize(40);
        if(completion){
            paintText.setTextSize(50);
            paintText.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));//bolds
        }
        c.drawText("STAGE 2 SCORE: "+this.score,0,c.getHeight()-100,paintText);
        c.drawText("LIVES REMAINING: "+GameActivity.lives,c.getWidth()-500,c.getHeight()-100,paintText);

        if(!block[0].blockGone)
            c.drawBitmap(this.block[0].picture,null,block[0].rect,paint);
        if(!block[1].blockGone)
            c.drawBitmap(this.block[1].picture,null,block[1].rect,paint);
        if(!block[2].blockGone)
            c.drawBitmap(this.block[2].picture,null,block[2].rect,paint);

        if(gameOverFlag){
            drawFailureScreen(c);
        }
        if(roundLostFlag){
            drawStageLostScreen(c);
        }

        if(completion){
            drawCompletionScreen(c);
        }
        drawMario(c, paint, ticks);
// Draw everything ( restricted to the displayed rectangle ) .
    }
    @Override
    public void tick ( Canvas c ) {

        //Needed so that we dont have songs playing over each other.


        this.mySong.start();

        if(collided){
            if(new String("Pipe").equals(collidedObject.type)){
                if(collisionSide == 0){
                    notAllowedRight = true;
                    notAllowedDown = false;
                }
                else if(collisionSide==1) {
                    notAllowedDown = false;
                    notAllowedLeft = true;
                }

                else if(collisionSide==2)
                    notAllowedDown = true;

                if(!oncePer) {
                    keyPadDetection(c);

                    //MARIO JUMP
                    if (jumpFlag)
                        marioJump();

                    //If the game has been started, these are the default movement values
                    horizontalSpeed = 15;
                    jumpSpeed = 20;
                    boo.move(horizontalSpeed / 3);
                    goomba.move(horizontalSpeed / 3);
                    star.move(horizontalSpeed/3);
                    oncePer = true;
                }
            }
            else if(new String("Boo").equals(collidedObject.type)) {
                if(collisionSide==2||mario.isInvincible){
                    boo.xPostrue = -500;//Not considered for collisionDetection anymore
                    booDead = true;
                    if(!onlyOnce) {
                        score += 150;
                        onlyOnce=true;
                    }
                }
                else {
                    if (this.mySong.isPlaying())
                        this.mySong.stop();
                    if (!playedBefore1) {
                        this.died.start();
                        playedBefore1 = true;
                    }
                    horizontalSpeed = 0;
                    jumpSpeed = 0;
                    marioMoving = false;
                    if(GameActivity.lives>=1){
                        roundLostFlag = true;
                        gameOverFlag = false;
                    }
                    else {
                        gameOverFlag = true;
                        roundLostFlag = false;
                    }
                }
            }
            else if(new String("Goomba").equals(collidedObject.type)) {
                if(collisionSide==2||mario.isInvincible){
                    goomba.xPostrue = -500;//Not considered for collisionDetection anymore
                    goombaDead = true;
                    if(!onlyOnce1) {
                        score += 150;
                        onlyOnce1=true;
                    }
                }
                else {
                    if (this.mySong.isPlaying())
                        this.mySong.stop();
                    if (!playedBefore1) {
                        this.died.start();
                        playedBefore1 = true;
                    }
                    horizontalSpeed = 0;
                    jumpSpeed = 0;
                    marioMoving = false;
                    if(GameActivity.lives>=1){
                        gameOverFlag=false;
                        roundLostFlag=true;
                    }
                    else {
                        gameOverFlag = true;
                        roundLostFlag=false;
                    }
                }
            }
            else if(new String("Block").equals(collidedObject.type)){
                if(collisionSide==3) {
                    score+=50;
                    block[collidedObject.numBlock].blockGone = true;
                    block[collidedObject.numBlock].xPostrue = -5000;
                    scoreChanged = true;
                }
            }
            else if(new String("Star").equals(collidedObject.type)) {
                mario.isInvincible = true;
                star.xPostrue = -5000;
            }
            collided = false;
        }



        if(!oncePer&&!gameOverFlag&&!roundLostFlag) {
            keyPadDetection(c);

            //MARIO JUMP
            if (jumpFlag)
                marioJump();
            else if(!notAllowedDown){
                if(collided) {
                    if ((mario.xPostrue + mario.xPostrue + 125) / 2 < (collidedObject).xPostrue || (mario.xPostrue + mario.xPostrue + 125) / 2 > (collidedObject).xPostrue + (collidedObject).width) {
                        notAllowedDown = false;
                    }
                }
                gravity();

            }

            //If the game has been started, these are the default movement values
            horizontalSpeed = 15;
            jumpSpeed = 15;
            boo.move(horizontalSpeed / 3);
            goomba.move(horizontalSpeed / 3);
            star.move(horizontalSpeed/3);
        }

        if(mario.xPostrue >= finishLine.xPostrue){
            completion = true;
            if(this.mySong.isPlaying())
                this.mySong.stop();
            if(!playedBefore2) {
                this.win.start();
                playedBefore2 = true;
            }
            horizontalSpeed = 0;
            jumpSpeed = 0;
            marioMoving = false;
        }
        //Draw all the bitmaps
        onDraw(c);
        if(scoreChanged){
            coinSound.start();
        }
        scoreChanged = false;
        oncePer = false;
        notAllowedLeft = false;
        notAllowedRight = false;
        notAllowedUp = false;
        //notAllowedDown = false;
        this.ticks++;
        //By default, mario is set to not be moving every tick so that he can only be moving if the
        //right arrow key is pressed
        marioMoving = false;
        collisionDetection();
    }

    public void collisionDetection(){
        loop:
        for(int i=0;i<objects.size();i++){
            if(Math.abs(mario.xPostrue-((SurfaceObjects)(objects.get(i))).xPostrue)<=125-10){
                if(Math.abs(mario.yPos-((SurfaceObjects)(objects.get(i))).yPos)<=((SurfaceObjects)objects.get(i)).length-10){
                    collidedObject = ((SurfaceObjects)objects.get(i));
                    collided = true;
                    if(((mario.yPos-125)<=((SurfaceObjects) (objects.get(i))).yPos)&&((mario.yPos-125)>=((SurfaceObjects) (objects.get(i))).yPos-((SurfaceObjects)objects.get(i)).length/2)&&new String("Block").equals(collidedObject.type)){
                        collisionSide = 3; //Bottom Collison
                        notAllowedUp = true;

                    }
                    else if(mario.yPos<=(((SurfaceObjects)objects.get(i)).yPos+(((SurfaceObjects)objects.get(i)).yPos-((SurfaceObjects)objects.get(i)).length))/2&&mario.yPos>=((SurfaceObjects)objects.get(i)).yPos-((SurfaceObjects)objects.get(i)).length){
                        collisionSide = 2; //TOP COLLISION

                    }
                    else if(mario.xPostrue <= (pipe.xPostrue+pipe.xPostrue+pipe.width)/2){//If mario position is less than the middle of the pipe's position, then he collided from the left
                        collisionSide = 0; //Left collision
                    }
                    else
                        collisionSide = 1; //Right collision
                    break loop;

                }

            }
            else{
                collisionSide = 4;//Random number
                notAllowedDown = false;
                notAllowedLeft = false;
                notAllowedRight= false;
            }
        }
    }
    public void marioJump() {

        if(notAllowedUp){
            playedBefore = false;
            jumpFlag = false;
            tempCount = 0;
        }
        else {
            if (!playedBefore) {
                this.jump.start(); //Sound effect
                playedBefore = true;
            }
            if (tempCount < 25) {
                mario.yPos -= jumpSpeed;
                mario.rect.bottom -= jumpSpeed;
                mario.rect.top -= jumpSpeed;
                tempCount++;
            } else if (notAllowedDown) {
                tempCount++;
                jumpFlag = false;
                tempCount = 0;
            } else if (tempCount >= 25 && tempCount < 50) {
                mario.yPos += jumpSpeed;
                mario.rect.bottom += jumpSpeed;
                mario.rect.top += jumpSpeed;
                tempCount++;
            }

            if (tempCount == 50) {
                playedBefore = false;
                jumpFlag = false;
                tempCount = 0;
            }
        }
    }


    public void keyPadDetection(Canvas c){
        //IF RIGHT ARROW IS PRESSED, MOVE MARIO TO RIGHT
        if(xtouch<=3*128&&xtouch>=256&&ytouch<=256&&ytouch>=128&&!notAllowedRight){

            MarioDirection = 1;
            mario.xPostrue+=horizontalSpeed;

            if(mario.xPos <= (c.getWidth()/2)){
                mario.xPos += horizontalSpeed;
                mario.rect.left += horizontalSpeed;
                mario.rect.right+= horizontalSpeed;
                marioMoving = true;
                if (actionUp) {
                    actionUp = false;
                    xtouch = 0;
                    ytouch = 0;
                }
            }
            else {
                boo.rect.left -= horizontalSpeed;
                boo.rect.right -= horizontalSpeed;
                boo.xPos -= horizontalSpeed;
                goomba.rect.left -= horizontalSpeed;
                goomba.rect.right -= horizontalSpeed;
                goomba.xPos -= horizontalSpeed;
                pipe.rect.left -= horizontalSpeed;
                pipe.rect.right -= horizontalSpeed;
                pipe.xPos -= horizontalSpeed;
                for(int i=0;i<3;i++) {
                    block[i].rect.left -= horizontalSpeed;
                    block[i].rect.right -= horizontalSpeed;
                    block[i].xPos -= horizontalSpeed;
                }
                star.rect.left -= horizontalSpeed;
                star.rect.right -= horizontalSpeed;
                star.xPos -= horizontalSpeed;
                cloud.rect.left -= 2;
                cloud.rect.right -= 2;
                cloud.xPos -= 2;
                finishLine.rect.left -= horizontalSpeed;
                finishLine.rect.right -= horizontalSpeed;
                finishLine.xPos -= horizontalSpeed;
                marioMoving = true;
                if (actionUp) {
                    actionUp = false;
                    xtouch = 0;
                    ytouch = 0;
                }
            }
        }
        //IF LEFT ARROW IS PRESSED MOVE MARIO TO LEFT
        else if(xtouch<=128&&xtouch>0&&ytouch<=256&&ytouch>=0&&!notAllowedLeft){
            MarioDirection = 0;
            boo.rect.left += horizontalSpeed;
            boo.rect.right += horizontalSpeed;
            boo.xPos+=horizontalSpeed;
            goomba.rect.left += horizontalSpeed;
            goomba.rect.right += horizontalSpeed;
            goomba.xPos+=horizontalSpeed;
            pipe.rect.left += horizontalSpeed;
            pipe.rect.right += horizontalSpeed;
            pipe.xPos += horizontalSpeed;
            for(int i=0;i<3;i++) {
                block[i].rect.left += horizontalSpeed;
                block[i].rect.right += horizontalSpeed;
                block[i].xPos += horizontalSpeed;
            }
            star.rect.left += horizontalSpeed;
            star.rect.right += horizontalSpeed;
            star.xPos+=horizontalSpeed;
            cloud.rect.left += 2;
            cloud.rect.right+= 2;
            cloud.xPos+=2;
            mario.xPostrue-=horizontalSpeed;
            finishLine.rect.left += horizontalSpeed;
            finishLine.rect.right += horizontalSpeed;
            finishLine.xPos += horizontalSpeed;
            marioMoving = true;
            if(actionUp){
                actionUp = false;
                xtouch = 0;
                ytouch = 0;
            }
        }
        //IF TOP ARROW IS PRESSED MAKE MARIO JUMP
        else if(xtouch<=256&&xtouch>128&&ytouch<=128&&ytouch>=0){
            this.jumpFlag = true;
            xtouch = 0;
            ytouch = 0;

        }
        //THIS IS REQUIRED SO THAT YOU CAN BE ABLE TO HOLD DOWN BUTTONS FOR CONTINOUS MOVEMENT
        else
            actionUp = false;
    }
    public void drawMario(Canvas c,Paint paint,int ticks){
        if(mario.isInvincible){
            if(jumpFlag){
                if (mario.picture != MainActivity.bitmap_jumping)
                    mario.picture = MainActivity.bitmap_jumping;
            }
            else if(ticks%10==0) {

                if(mario.picture == MainActivity.bitmap_black_mario){
                    mario.picture = MainActivity.bitmap_green_mario;
                }
                else{
                    mario.picture = MainActivity.bitmap_black_mario;
                }
            }
            c.drawBitmap(this.mario.picture, null, this.mario.rect, paint);
        }
        else {
            if (jumpFlag) {
                if (mario.picture != MainActivity.bitmap_jumping)
                    mario.picture = MainActivity.bitmap_jumping;
                c.drawBitmap(this.mario.picture, null, this.mario.rect, paint);
            } else if (!marioMoving) {
                if (mario.picture != MainActivity.bitmap_stationary)
                    mario.picture = MainActivity.bitmap_stationary;
                c.drawBitmap(this.mario.picture, null, this.mario.rect, paint);

            } else {
                if (ticks % 10 == 0) {
                    if (MarioDirection == 1) {
                        if (mario.picture == MainActivity.bitmap_mario_running1)
                            mario.picture = MainActivity.bitmap_mario_running2;
                        else if (mario.picture == MainActivity.bitmap_mario_running2)
                            mario.picture = MainActivity.bitmap_mario_running3;
                        else if (mario.picture == MainActivity.bitmap_mario_running3)
                            mario.picture = MainActivity.bitmap_mario_running1;
                        else
                            mario.picture = MainActivity.bitmap_mario_running1;
                    } else {
                        if (mario.picture == MainActivity.bitmap_mario_running1_1)
                            mario.picture = MainActivity.bitmap_mario_running2_1;
                        else if (mario.picture == MainActivity.bitmap_mario_running2_1)
                            mario.picture = MainActivity.bitmap_mario_running3_1;
                        else if (mario.picture == MainActivity.bitmap_mario_running3_1)
                            mario.picture = MainActivity.bitmap_mario_running1_1;
                        else
                            mario.picture = MainActivity.bitmap_mario_running1_1;
                    }
                }
                c.drawBitmap(this.mario.picture, null, this.mario.rect, paint);
            }
        }
    }
    public void drawCompletionScreen(Canvas c){
        if(!onlyOnce3) {
            GameActivity.score+=score;
            onlyOnce3=true;
        }
        Paint paint3 = new Paint() ;
        paint3 . setAlpha(255) ; // Control transparency
        paint3.setColor(Color.GREEN);
        paint3.setTextSize(100);
        paint3.setTypeface(Typeface.DEFAULT_BOLD);
        c.drawText("STAGE COMPLETE!", c.getWidth() / 2 - 360, c.getHeight() / 2 - 100, paint3);
        paint3.setTextSize(50);
        c.drawText("Press the Back Button to Return to Menu!",c.getWidth()/2-400,c.getHeight()/2+25,paint3);
        c.drawText("ROUND SCORE : " + score, c.getWidth() / 2 - 150, c.getHeight() / 2 + 100, paint3);
        c.drawText("TOTAL SCORE : "+GameActivity.score,c.getWidth()/2-150,c.getHeight()/2+200,paint3);
    }
    public void drawFailureScreen(Canvas c){
        Paint paint2 = new Paint() ;
        paint2 . setAlpha(255) ; // Control transparency
        paint2.setColor(Color.RED);
        paint2.setTextSize(100);
        paint2.setTypeface(Typeface.DEFAULT_BOLD);
        c.drawText("GAME OVER", c.getWidth() / 2 - 250, c.getHeight() / 2 - 100, paint2);
        paint2.setTextSize(50);
        c.drawText("Thank You For Playing!", c.getWidth() / 2 - 200, c.getHeight() / 2 + 25, paint2);
        paint2.setColor(Color.GREEN);
        c.drawText("ROUND SCORE : " + score, c.getWidth() / 2 - 150, c.getHeight() / 2 + 100, paint2);
        c.drawText("TOTAL SCORE : "+GameActivity.score,c.getWidth()/2-150,c.getHeight()/2+200,paint2);
    }
    public void drawStageLostScreen(Canvas c) {
        if (!onlyOnce4) {
            GameActivity.lives--;
            GameActivity.score += score;
            onlyOnce4 = true;
        }
        Paint paint2 = new Paint();
        paint2.setAlpha(255); // Control transparency
        paint2.setColor(Color.RED);
        paint2.setTextSize(100);
        paint2.setTypeface(Typeface.DEFAULT_BOLD);
        c.drawText("Stage Lost", c.getWidth() / 2 - 245, c.getHeight() / 2 - 100, paint2);
        paint2.setTextSize(50);
        c.drawText("Press the Back Button to Continue!", c.getWidth() / 2 - 350, c.getHeight() / 2 + 25, paint2);
        paint2.setColor(Color.GREEN);
        c.drawText("STAGE SCORE : " + score, c.getWidth() / 2 - 175, c.getHeight() / 2 + 100, paint2);
        c.drawText("TOTAL SCORE : " + GameActivity.score, c.getWidth() / 2 - 175, c.getHeight() / 2 + 200, paint2);
    }
    public void gravity(){
        if(mario.yPos<this.grass.rectangle1.top){
            mario.yPos+=jumpSpeed;
            mario.rect.bottom+=jumpSpeed;
            mario.rect.top+=jumpSpeed;

        }
        else{
            mario.yPos = this.grass.rectangle1.top;
            mario.rect.bottom = this.grass.rectangle1.top;
            mario.rect.top = this.grass.rectangle1.top-125;
        }
    }
}


